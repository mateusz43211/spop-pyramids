module Common where
import Data.List

-- funkcja zwracająca listę elementów, które występują w obu wejściowych listach
commonElements :: Eq a => [a] -> [a] -> [a]
commonElements _ [] = []
commonElements [] _ = []
commonElements (x:xs) second | elem x second  = x:(commonElements xs newSecond)
                             | otherwise      = commonElements xs newSecond
                             where newSecond = delete x second

itselfL :: a -> a
itselfL val = val

-- funkcja wykonująca transpozycję zagnieżdżonych list
transposeL :: [[a]] -> [[a]]
transposeL ([]:_) = []
transposeL x = (map head x) : transposeL (map tail x)

-- funkcja wykonująca odwrócenie zagnieżdżonych list
reverseL :: [[a]] -> [[a]]
reverseL [] = []
reverseL x = map reverse x

-- funkcja sprawdzająca, czy wszystkie kolummny mają unikalne wartości
allColumnsUnique :: (Eq a) => [[a]] -> Bool
allColumnsUnique values = result
                          where
                            columns = transposeL values
                            result = all (==True) (map allDifferent columns)

-- funkcja obliczająca iloczyn kartezjański elementów wejściowych
cartesianProduct :: [[a]] -> [[a]]
cartesianProduct value = foldr f [[]] value
                          where f l a = [ x:xs | x <- l, xs <- a ]

-- funkcja sprawdzająca, czy każdy element listy jest unikalny
allDifferent :: (Eq a) => [a] -> Bool
allDifferent list = case list of
    []      -> True
    (x:xs)  -> x `notElem` xs && allDifferent xs

-- funkcja łącząca elementy zagnieżdżonych list w krotkę
combine :: [[a]] -> [[b]] -> [[(a,b)]]
combine [] _ = []
combine _ [] = []
combine (x:xs) (y:ys) = [(zip x y)] ++ (combine xs ys)