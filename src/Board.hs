module Board where
import Common

type Height = Maybe Int
type Coord = (Int, Int)
data Pyramid = Pyramid Coord Height deriving Show
data Board = Board [[Pyramid]]

-- funkcja tworząca pustą planszę łamigłówki
createEmptyBoard :: Int -> Board
createEmptyBoard size = Board (map (map emptyPyramid) (createCoordBoard size))

createCoordBoard :: Int -> [[Coord]]
createCoordBoard size = map (createCoordRow values) values
                        where values = [0..(size-1)]

createCoordRow :: [Int] -> Int -> [Coord]
createCoordRow list first = map (\val -> (first, val)) list

emptyPyramid :: Coord -> Pyramid
emptyPyramid coord = Pyramid coord Nothing

-- funkcja wypełniająca planszę podanymi wartościami
fillBoardWithValues :: Board -> [[Int]] -> Board
fillBoardWithValues (Board pyramids) values = Board result
                                               where
                                                tuples = combine pyramids values
                                                result = map (map(\(a,b) -> setValueOfPyramid a b)) tuples
-- funkcja ustawiająca wysokość piramidy
setValueOfPyramid :: Pyramid -> Int -> Pyramid
setValueOfPyramid (Pyramid coord _) newHeight = (Pyramid coord (Just newHeight))


instance Show Board where
  show board = showBoard board

-- funkcja zwracająca napis reprezentujący piramidę
showPyramid :: Pyramid -> String
showPyramid (Pyramid _ (Just x)) = show x
showPyramid (Pyramid _ (Nothing)) = "*"

-- funkcja zwracająca napis reprezentujący wiersz planszy łamigłówki
showRow :: [Pyramid] -> String
showRow points = (splitWithSeparator (concat (map showPyramid points)) "\t" )++ "\n"

-- funkcja zwracająca napis reprezentujący planszę łamigłówki
showBoard :: Board -> String
showBoard (Board pyramids) = columnNames ++ rowsWithNumbers
                  where
                    columnNames = "\t" ++ (concat rowNumbers) ++ "\n"
                    rowsWithNumbers = concat (zipWith (++) rowNumbers mappedRows)
                    rowNumbers = map (++"|\t") (numbersRange 0 (length pyramids - 1))
                    mappedRows = map showRow pyramids

-- funkcja dzieląca łańcuch znaków podanym separatorem
splitWithSeparator :: String -> String -> String
splitWithSeparator [] _             = []
splitWithSeparator [x] _            = [x]
splitWithSeparator (x:xs) separator = [x] ++ separator ++ splitWithSeparator xs separator

-- funkcja generująca zakres liczb zamieniony na napis
numbersRange :: Int -> Int -> [String]
numbersRange from to = map show (takeWhile (<=to) [from..])
numbersRange from to = map show [x | x <- [from..], x < to]