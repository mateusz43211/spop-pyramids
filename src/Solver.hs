module Solver where
import Board
import Common
import Constraints

-- funkcja rozwiązująca łamigłówkę
solve :: GameConstraints -> [Board]
solve gameConstraints = newResult
                        where
                          board = createEmptyBoard $ getSize gameConstraints -- stworzenie pustej planszy
                          allPossibleValues = mapBoardToPossibleValues gameConstraints board -- wyznaczenie wszystkich możliwych wartości dla każdej komórki planszy
                          possibleRows = map (\list -> filter allDifferent (cartesianProduct list)) allPossibleValues -- wyznaczenie wszystkich możliwych ustawień wierszy (z unikalnymi symbolami)
                          allPossibleBoards = cartesianProduct possibleRows -- wyznaczenie wszystkich możliwych plansz
                          possibleBoardsWithUniqueColumns = filter allColumnsUnique allPossibleBoards -- przefiltrowanie plansz poporzez sprawdzenie unikalności elementów w wierszach
                          result = validate gameConstraints res -- przefiltrowanie plansz poporzez dopasowanie do ograniczeń łamigłówki
                          newResult = map (fillBoardWithValues board) result -- zmapowanie wyników wyszukiwania na planszę 

-- funkcja zwracająca rozmiar planszy
getSize :: GameConstraints -> Int
getSize (GameConstraints top _ _ _) = length top

-- funkcja mapująca ograniczenia łamigłówki na możliwe wartości we wszystkich komórkach planszy
mapBoardToPossibleValues :: GameConstraints -> Board -> [[[Int]]]
mapBoardToPossibleValues gameConstraints (Board pyramids) = map (map $ toPossibleValues gameConstraints) pyramids

toPossibleValues :: GameConstraints -> Pyramid -> [Int]
toPossibleValues gameConstraints (Pyramid coord _ ) = getPossibleValues coord constraint maxHeight
                                                      where
                                                        maxHeight = getSize gameConstraints
                                                        constraint = getConstraintForCoord coord gameConstraints

-- funkcja zwracająca ograniczenia mające wpływ na podaną komórkę
getConstraintForCoord :: Coord -> GameConstraints -> Constraint
getConstraintForCoord (x,y) (GameConstraints top bottom left right) =  Constraint nTop nBottom nLeft nRight
                                                                       where
                                                                        nTop = top !! y
                                                                        nBottom = bottom !! y
                                                                        nLeft = left !! x
                                                                        nRight =  right !! x

-- funkcja zwracająca lisę możliwych wartości dla danej komórki
getPossibleValues :: Coord -> Constraint -> Int -> [Int]
getPossibleValues (x,y) (Constraint top bottom left right) maxHeight  = foldr commonElements fromTop [fromBottom, fromLeft, fromRight] -- możliwe są tylko takie wartości, które spełniają wszystkie ograniczenia
                                                                        where
                                                                         fromTop    = mapConstraint top (calcDistanceFromConstraint (x,y) STop maxHeight) maxHeight
                                                                         fromBottom = mapConstraint bottom (calcDistanceFromConstraint (x,y) SBottom maxHeight) maxHeight
                                                                         fromLeft   = mapConstraint left (calcDistanceFromConstraint (x,y) SLeft maxHeight) maxHeight
                                                                         fromRight  = mapConstraint right (calcDistanceFromConstraint (x,y) SRight maxHeight) maxHeight

-- funkcja zwracająca możliwe wartości dla komórki na postawie wartości ogarniczenia
mapConstraint :: Maybe Int -> Int -> Int -> [Int]
mapConstraint (Nothing) distanceFromConstraint maxHeight = [1..maxHeight]
mapConstraint (Just x) distanceFromConstraint maxHeight = possibleValuesByConstraintValue x distanceFromConstraint maxHeight

-- funkcja zwracająca możliwe wartości dla komórki na postawie wartości ogarniczenia
possibleValuesByConstraintValue :: Int -> Int -> Int -> [Int]
possibleValuesByConstraintValue constraintValue distanceFromConstraint maxHeight | constraintValue == 1          = if distanceFromConstraint == 0 then [maxHeight] else [1..(maxHeight - 1)]
                                                                                 | constraintValue == maxHeight  = [(distanceFromConstraint + 1)]
                                                                                 | otherwise                     = [1..(min maxHeight (maxHeight + 1 - constraintValue + distanceFromConstraint))]
-- funkcja zwracająca odległość punktu od ograniczenia (jeśli punkt jest przy ograniczeniu to odległość wynosi 0)
calcDistanceFromConstraint :: (Int,Int) -> Side -> Int -> Int
calcDistanceFromConstraint (x,y) side maxHeight = case side of
                                                    STop -> x
                                                    SBottom -> (maxHeight - x) - 1
                                                    SLeft -> y
                                                    SRight -> (maxHeight - y) - 1

-- funkcja filtrująca możliwe rozwiązania na podstawie spełnienia wszystkich ograniczeń łamigłówki
validate :: GameConstraints -> [[[Int]]] -> [[[Int]]]
validate (GameConstraints top bottom left right) values = filteredByRight
                                                        where
                                                          filteredByTop = filterByConstraint top STop values
                                                          filteredByBottom = filterByConstraint bottom SBottom filteredByTop
                                                          filteredByLeft = filterByConstraint left SLeft filteredByBottom
                                                          filteredByRight = filterByConstraint right SRight filteredByLeft

-- funkcja filtrująca możliwe rozwiązania planszy na podstawie podanych ograniczeń łamigłówki
filterByConstraint :: [Maybe Int] -> Side -> [[[Int]]] -> [[[Int]]]
filterByConstraint constraints side ops = result
                                          where
                                            transform = transformForSide side       -- transformacje są po to, aby zliczać odpowiednio widoczne piramidy z danego kierunku
                                            inverseTransform = inverseTransformForSide side
                                            tupledWithConstraint = map (zip constraints) (map transform ops)
                                            filteredTuplesWithConstraint = filter allValidForConstraint tupledWithConstraint -- przefiltrowanie możliwych rozwiązań sprawdzające czy spełniają one wszystkie ograniczenia
                                            untupledResult = map (map snd) filteredTuplesWithConstraint
                                            result = map inverseTransform untupledResult

-- funkcja zwracająca transformację elementów dla podanego kierunku
transformForSide :: Side -> ([[Int]] -> [[Int]])
transformForSide side = case side of
                           STop -> transposeL
                           SBottom -> reverseL . transposeL
                           SLeft -> itselfL
                           SRight -> reverseL

-- funkcja zwracająca odwortną transformację elementów dla podanego kierunku
inverseTransformForSide :: Side -> ([[Int]] -> [[Int]])
inverseTransformForSide side = case side of
                                 STop -> transposeL
                                 SBottom -> transposeL . reverseL
                                 SLeft -> itselfL
                                 SRight -> reverseL

-- funkcja sprawdzająca, czy wszystkie listy spełniają ograniczenia łamigłówki
allValidForConstraint :: [(Maybe Int, [Int])] -> Bool
allValidForConstraint values =  all (== True) validatedValues
                                where
                                  validatedValues = map validForConstraint values

-- funkcja spradzająca, czy lista wartości spełnia ograniczenie łamigłówki
validForConstraint :: (Maybe Int, [Int]) -> Bool
validForConstraint (Nothing, _) = True
validForConstraint ((Just x), list) = x == (countVisiblePyramids list)

-- funkcja zliczająca widoczne piramidy
countVisiblePyramids :: [Int] -> Int
countVisiblePyramids [] = 0
countVisiblePyramids (x:xs) = 1 + countPyramids (x:xs) x

-- funkcja zliczająca, ile razy znaleziono wyższą piramidę niż podana
countPyramids :: [Int] -> Int -> Int
countPyramids [] highestPyramid                           = 0
countPyramids (x:xs) highestPyramid | x > highestPyramid  = 1 + countPyramids xs x
                                    | otherwise           = countPyramids xs highestPyramid