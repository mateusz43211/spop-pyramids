module Main where
import Solver
import Board
import Constraints

{- Główna funkcja - wczytuje ograniczenia łamigłówki z pliku oraz sprawdza ich poprawność.
   Gdy ograniczenia są poprawne, rozpoczyna poszukiwanie rozwiązania, a gdy je znajdzie, jest wyświetlane
   w konsoli -}

main :: IO ()
main = do
  putStrLn "Enter the file name:"
  filename <- getLine
  content <- readFile filename

  let loadedConstraints = read content::GameConstraints

  if (validateConstraints loadedConstraints) then do
      putStrLn "The correct puzzle has been loaded"
    else
      putStrLn "An incorrect puzzle was loaded"

  putStrLn "Loaded constraints"
  putStrLn $ show loadedConstraints

  putStrLn "Puzzle solution"
  let solverResult = solve loadedConstraints
  if (length solverResult) == 1 then
    do
      putStrLn "Puzzle solution found"
      putStrLn $ show solverResult
  else if null solverResult then putStrLn "Puzzle solution found"
  else do
        putStrLn "Puzzle has more than one solution:"
        putStrLn $ show $ length solverResult
        printElements solverResult
        putStrLn "The end"

printElements :: [Board] -> IO()
printElements [] = return ()
printElements (x:xs) = do putStrLn $ show x
                          printElements xs