module Constraints where

-- typ danych przechowujący wszystkie ograniczenia łamigłowki
data GameConstraints = GameConstraints [Maybe Int] [Maybe Int] [Maybe Int] [Maybe Int] deriving (Show, Read)

-- typ danych reprezentujący ogranizcenia dla komórki planszy łamigłówki
data Constraint = Constraint (Maybe Int) (Maybe Int) (Maybe Int) (Maybe Int) deriving (Show, Read)

-- typ danych do reprezentacji położenia ograniczenia (góra, dół, lewo, prawo)
data Side = STop | SBottom | SLeft | SRight

-- funkcja sprawdzająca poprawność wczytanych ograniczeń łamigłówki
validateConstraints :: GameConstraints -> Bool
validateConstraints (GameConstraints top bottom left right) = allWithSize size [top, bottom, left, right] && allWithValueInRange 1 size [top, bottom, left, right]
                                                              where size = length top
-- funkcja sprawdzająca czy wszystkie list mają podany rozmiar
allWithSize :: Int -> [[a]] -> Bool
allWithSize size lists = all (\list -> (length list) == size) lists

-- funkcja sprawdzająca czy wszystkie elemnty znajdują się w podanym zakresie
allWithValueInRange :: Int -> Int -> [[Maybe Int]] -> Bool
allWithValueInRange min max lists = all (\list -> all (\val -> isValueInRange val min max) list) lists

-- funkcja sprawdzająca, czy wartość znajduje się w podanym zakresie
isValueInRange :: Maybe Int -> Int -> Int -> Bool
isValueInRange Nothing _ _ = True
isValueInRange (Just val) min max = min <= val && val <= max